/**
 ******************************************************************************
 * @file    main.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    09-March-2023
 * @brief   main for the accelerometer logger.
 *          Based on TI resource explorer example "M2_SDCardReader"
 *          found in MSP430Ware 3.80.14.01
 ******************************************************************************
 * @copyright
 *
 * License information
 *
 ******************************************************************************
 */

/* --COPYRIGHT--,BSD
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

/*  
 * ======== main.c ========
 * Mass Storage with SDCARD.
 *
 * This example demonstrates usage of the API with file system software.  
 * It includes an MSP430 port of the open-source "FatFs" software for the FAT 
 * file system.  
 * This example requires hardware with an SD-card interface.  At the time of 
 * writing, the only such hardware TI provdies is the F5529 Experimenter?s 
 * Board, available from TI?s eStore.
 *
 +----------------------------------------------------------------------------+
 * Please refer to the Examples Guide for more details.
 *----------------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>

#include "driverlib.h"

#include "main.h"

#include "leds.h"
#include "sdcard.h"
#include "switch.h"

/* Private define ------------------------------------------------------------*/

#define FILENAME "log.txt"

#define START_MESSAGE "start\n"
#define STOP_MESSAGE  "stop\n"
#define EVENT_MESSAGE "event\n"
#define TEST_MESSAGE  "test\n"

#define ACLK_FREQUENCY 32768
#define MEASURE_PERIOD 512 // period in 1/32768 s

#define EVENT_TIMEOUT 64 // period in MEASURE_PERIOD/32768 s

#define SWITCH_ON SWITCH_S2
#define SWITCH_OFF SWITCH_S1
#define LED_ON LED3
#define LED_ERROR LED1
#define LED_EVENT LED2

/* Private variables ---------------------------------------------------------*/

static Timer_A_initUpModeParam Timer_A_params = {0};

static volatile bool measure_flag = false;
static bool measure_enable = false;

static uint16_t event_countdown = 0;

/* Private functions declaration ---------------------------------------------*/

static void initTimer(void);
static void setTimer_A_Parameters(void);

/* Public functions ----------------------------------------------------------*/

/*  
 * ======== main ========
 */
void main (void)
{
    WDT_A_hold(WDT_A_BASE); // Stop watchdog timer

    // Minumum Vcore setting required for the USB API is PMM_CORE_LEVEL_2 .
    PMM_setVCore(PMM_CORE_LEVEL_2);
    initTimer();

    switch_init();
    leds_init();

    __enable_interrupt();       // Enable interrupts globally

    while (1)
    {
        if (switch_isPressed(SWITCH_ON) && !measure_enable) {
            measure_enable = true;
            leds_set(LED_ON, true);
            sdcard_mount();
            sdcard_open_file(FILENAME);
            sdcard_write_file(START_MESSAGE, strlen(START_MESSAGE));
            event_countdown = EVENT_TIMEOUT;
        }
        if (switch_isPressed(SWITCH_OFF) && measure_enable) {
            measure_enable = false;
            leds_set(LED_ON, false);
            sdcard_write_file(STOP_MESSAGE, strlen(STOP_MESSAGE));
            sdcard_close_file();
            sdcard_unmount();
        }
        if (switch_isPressed(SWITCH_ON) && measure_enable && (event_countdown == 0)) {
            leds_set(LED_EVENT, true);
            event_countdown = EVENT_TIMEOUT;
            sdcard_write_file(EVENT_MESSAGE, strlen(EVENT_MESSAGE));
        }
        if (sdcard_get_last_error() != FR_OK) {
            leds_set(LED_ERROR, true);
        }

        if (measure_flag && measure_enable) {
            measure_flag = false;
            sdcard_write_file((uint8_t*)TEST_MESSAGE, strlen(TEST_MESSAGE));
        }
    }  //while(1)

}

/*  
 * ======== TIMER0_A0_ISR ========
 */
#if defined(__TI_COMPILER_VERSION__) || (__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR (void)
#elif defined(__GNUC__) && (__MSP430__)
void __attribute__ ((interrupt(TIMER0_A0_VECTOR))) TIMER0_A0_ISR (void)
#else
#error Compiler not found!
#endif
{
    if (event_countdown > 0) {
        event_countdown--;
        if (event_countdown == 0) {
            leds_set(LED_EVENT, false);
        }
    }

    measure_flag = true;
    
    //Wake from ISR, if sleeping
    __bic_SR_register_on_exit(LPM0_bits);
}

/*  
 * ======== UNMI_ISR ========
 */
#if defined(__TI_COMPILER_VERSION__) || (__IAR_SYSTEMS_ICC__)
#pragma vector = UNMI_VECTOR
__interrupt void UNMI_ISR (void)
#elif defined(__GNUC__) && (__MSP430__)
void __attribute__ ((interrupt(UNMI_VECTOR))) UNMI_ISR (void)
#else
#error Compiler not found!
#endif
{
    switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG))
    {
        case SYSUNIV_NONE:
            __no_operation();
            break;
        case SYSUNIV_NMIIFG:
            __no_operation();
            break;
        case SYSUNIV_OFIFG:
            UCS_clearFaultFlag(UCS_XT2OFFG);
            UCS_clearFaultFlag(UCS_DCOFFG);
            SFR_clearInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);
            break;
        case SYSUNIV_ACCVIFG:
            __no_operation();
            break;
        case SYSUNIV_BUSIFG:
            // If the CPU accesses USB memory while the USB module is
            // suspended, a "bus error" can occur.  This generates an NMI.  If
            // USB is automatically disconnecting in your software, set a
            // breakpoint here and see if execution hits it.  See the
            // Programmer's Guide for more information.
            SYSBERRIV = 0; //clear bus error flag
    }
}

/* Private functions implementation ------------------------------------------*/

/*
 * ======== setTimer_A_Parameters ========
 */
// This function sets the timer A parameters
static void setTimer_A_Parameters()
{
    Timer_A_params.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
	Timer_A_params.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    Timer_A_params.timerPeriod = MEASURE_PERIOD - 1;
	Timer_A_params.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
	Timer_A_params.captureCompareInterruptEnable_CCR0_CCIE =
		       TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE;
	Timer_A_params.timerClear = TIMER_A_DO_CLEAR;
	Timer_A_params.startTimer = false;
}


static void initTimer(void)
{

    setTimer_A_Parameters();   

    //start timer
    Timer_A_clearTimerInterrupt(TIMER_A0_BASE);

    Timer_A_initUpMode(TIMER_A0_BASE, &Timer_A_params);

    Timer_A_startCounter(TIMER_A0_BASE,
        TIMER_A_UP_MODE);
}
//Released_Version_5_20_06_02
