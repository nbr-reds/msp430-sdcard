# msp430-sdcard
Demonstrate the use of a SD card with msp430.

## Hardware
MSP-EXP430F5529 with a SD card.

## Development environment
Code Composer Studio 12.2.0 with MSP430ware 3.80.14.01.
For the installation, follow the [instructions](Experimenter Board Installation Guide_2023.pdf).

## Description
- S2 activate the measure, if the measure is already activated, an event is logged.
- S1 desactivate the measure.
- LED3 (green) indicate when the measure is active.
- LED2 (orange) indicate an event.
- LED1 (red) indicate an error with the SD card.
- A message is logged when measure start or stop.
- When measure is activated, "test" is logged at 64 samples per second into log.txt.
- SD card can be safely removed when the measure is desactivated.