/**
 ******************************************************************************
 * @file    sdcard.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    09-March-2023
 * @brief   SD card management
 ******************************************************************************
 * @copyright
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef SDCARD_H
#define SDCARD_H

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

#include "FatFs/ff.h"

/* Exported functions --------------------------------------------------------*/

/**
 * Mount the SD card
 * @return File function return code
 */
FRESULT sdcard_mount(void);

/**
 * Unmount theSD card
 * @return File function return code
 */
FRESULT sdcard_unmount(void);


/**
 * Open the file in append mode
 * @param filename the filename
 * @return File function return code
 */
FRESULT sdcard_open_file(const char* filename);

/**
 * Close the file
 * @return File function return code
 */
FRESULT sdcard_close_file(void);

/**
 * Write data
 * @param data the data to write
 * @param size the size of the data to write
 * @return File function return code
 */
FRESULT sdcard_write_file(const uint8_t* data, uint8_t size);

/**
 * Get the file size
 * @return the file size
 */
uint32_t sdcard_get_filesize(void);

/**
 * Get the free size in the SD card
 * @return the free size
 */
uint64_t sdcard_get_free_size(void);

/**
 * Get the last error
 * @return File function return code
 */
FRESULT sdcard_get_last_error(void);

#endif
