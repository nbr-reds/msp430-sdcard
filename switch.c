/**
 ******************************************************************************
 * @file    switch.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    23-March-2020
 * @brief   switch
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <msp430.h>
#include <stdbool.h>
#include <stdint.h>

#include "switch.h"

/* Private define ------------------------------------------------------------*/

#define SWITCH_COUNT 2

#define S1_DIR P1DIR
#define S1_IN  P1IN
#define S1_BIT BIT7
#define S1_REN P1REN
#define S1_OUT P1OUT

#define S2_DIR P2DIR
#define S2_IN  P2IN
#define S2_BIT BIT2
#define S2_REN P2REN
#define S2_OUT P2OUT

/* Public functions ----------------------------------------------------------*/

void switch_init(void)
{
    // Init switch S1
    S1_DIR &= ~S1_BIT;
    S1_REN |= S1_BIT;
    S1_OUT |= S1_BIT;

    // Init switch S2
    S2_DIR &= ~S2_BIT;
    S2_REN |= S2_BIT;
    S2_OUT |= S2_BIT;
}

bool switch_isPressed(switch_t index) {
    if (index == SWITCH_S1) {
        return (S1_IN & S1_BIT) == 0;
    } else if (index == SWITCH_S2) {
        return (S2_IN & S2_BIT) == 0;
    } else {
        return false;
    }
}
