/**
 ******************************************************************************
 * @file    sdcard.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    09-March-2023
 * @brief   SD card management
 ******************************************************************************
 * @copyright
 *
 * License information
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <ctype.h>
#include <msp430.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "sdcard.h"

/* Private define ------------------------------------------------------------*/

#define SYNC_FAT_PERIOD 60

/* Private variables ---------------------------------------------------------*/

static FATFS fatfs;   /* File system object */
static FIL file;
static uint8_t syncFatCounter = 0;
static FRESULT lastError;

/* Public functions ----------------------------------------------------------*/

FRESULT sdcard_mount(void) {
    lastError = f_mount(0, &fatfs);
    return lastError;
}

FRESULT sdcard_unmount(void) {
    lastError = f_mount(0, NULL);
    return lastError;
}

FRESULT sdcard_open_file(const char* filename) {
    lastError = f_open(&file, filename, FA_WRITE | FA_OPEN_ALWAYS);
    if (lastError == FR_OK) lastError = f_lseek(&file, file.fsize);
    return lastError;
}

FRESULT sdcard_close_file(void) {
    lastError = f_close(&file);
    return lastError;
}

FRESULT sdcard_write_file(const uint8_t* data, uint8_t size) {
    uint16_t bw;

    lastError = f_write(&file, data, size, &bw);
    if (lastError == FR_OK) {
        if (++syncFatCounter == SYNC_FAT_PERIOD) {
            syncFatCounter = 0;
            lastError = f_sync(&file);
        }
    }
    return lastError;
}

uint32_t sdcard_get_filesize(void) {
    return f_size(&file);
}

uint64_t sdcard_get_free_size(void) {
    uint32_t freeCluster;
    FATFS *fs;
    lastError = f_getfree(NULL, &freeCluster, &fs);

    return (uint64_t)freeCluster * fs->csize * 512;
}

FRESULT sdcard_get_last_error(void) {
    return lastError;
}
