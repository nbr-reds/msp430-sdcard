/**
 ******************************************************************************
 * @file    leds.c
 * @author  nicolas.brunner@heig-vd.ch
 * @date    31-March-2021
 * @brief   Leds driver
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/

#include <msp430.h>

#include "leds.h"

/* Private variables ---------------------------------------------------------*/

static const uint8_t ports[LEDS_COUNT] = {1, 8, 8, 1, 1, 1, 1, 1};
static const uint8_t pins[LEDS_COUNT] = {BIT0, BIT1, BIT2, BIT1, BIT2, BIT3, BIT4, BIT5};

/* Public functions ----------------------------------------------------------*/

void leds_init(void)
{
    for (int i = 0; i < LEDS_COUNT; i++) {
        if (ports[i] == 1) {
            P1DIR |= pins[i];
        } else if (ports[i] == 8) {
            P8DIR |= pins[i];
        }

        leds_set(i, false);
    }
}

void leds_set(int led_index, bool value)
{
    if (ports[led_index] == 1) {
        if (value) {
            P1OUT |= pins[led_index];
        } else {
            P1OUT &= ~pins[led_index];
        }
    } else if (ports[led_index] == 8) {
        if (value) {
            P8OUT |= pins[led_index];
        } else {
            P8OUT &= ~pins[led_index];
        }
    }
}

void leds_toggle(int led_index)
{
    if (ports[led_index] == 1) {
        P1OUT ^= pins[led_index];
    } else if (ports[led_index] == 8) {
        P8OUT ^= pins[led_index];
    }
}
