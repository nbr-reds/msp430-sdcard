/**
 ******************************************************************************
 * @file    leds.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    31-March-2021
 * @brief   Leds driver
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

#ifndef LEDS_H
#define LEDS_H

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdint.h>

/* Private define ------------------------------------------------------------*/

#define LEDS_COUNT 8

#define LED1     0
#define LED2     1
#define LED3     2
#define LED_PAD1 3
#define LED_PAD2 4
#define LED_PAD3 5
#define LED_PAD4 6
#define LED_PAD5 7

/* Exported functions --------------------------------------------------------*/

void leds_init(void);

void leds_set(int led_index, bool value);

void leds_toggle(int led_index);

#endif
