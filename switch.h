/**
 ******************************************************************************
 * @file    switch.h
 * @author  nicolas.brunner@heig-vd.ch
 * @date    23-March-2020
 * @brief   switch
 ******************************************************************************
 * @copyright HEIG-VD
 *
 * License information
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef SWITCH_H
#define SWITCH_H

/* Includes ------------------------------------------------------------------*/

#include <stdint.h>

/* Exported types ------------------------------------------------------------*/

typedef enum {
    SWITCH_S1,
    SWITCH_S2,
} switch_t;

/* Exported functions --------------------------------------------------------*/

/**
 * Init the switch
 */
void switch_init(void);

bool switch_isPressed(switch_t index);

#endif
